﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace org.mardld.consoleApp1
{
    public interface IChannelFactoryProvider<TChannel>
    {
        IChannelFactoryWrapper<TChannel> GetChannelFactory(Binding binding, EndpointAddress remoteAddress);
    }
}
