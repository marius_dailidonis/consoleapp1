﻿using System;

namespace org.mardld.consoleApp1
{
    public interface IFriendFindServiceClient
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        TestResponse Test();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longPollUrl"></param>
        /// <returns></returns>
        TestResponse GetFriend(Uri longPollUrl);
    }
}
