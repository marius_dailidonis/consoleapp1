﻿using RestSharp;
using System;
using System.ServiceModel;

namespace org.mardld.consoleApp1
{
    /// <summary>
    /// 
    /// </summary>
    public class FriendFindServiceClient
    {
        private readonly EndpointAddress _remoteAddress;
        private readonly WebHttpBinding _webBinding;

        private readonly WebHttpBinding _longPollWebBinding;

        private readonly IChannelFactoryProvider<IFriendFindService> _channelFactoryProvider;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="timeout"></param>
        /// <param name="channelFactoryProvider"></param>
        public FriendFindServiceClient(string url, int timeout, IChannelFactoryProvider<IFriendFindService> channelFactoryProvider)
        {
            _channelFactoryProvider = channelFactoryProvider;

            _remoteAddress = new EndpointAddress(url);
            _webBinding = new WebHttpBinding()
            {
                AllowCookies = true,
                BypassProxyOnLocal = true,
                SendTimeout = TimeSpan.FromSeconds(timeout),
            };

            _longPollWebBinding = new WebHttpBinding()
            {
                AllowCookies = true,
                BypassProxyOnLocal = true,
                SendTimeout = TimeSpan.FromSeconds(60)
            };

        }

        public TestResponse Test2()
        {
            var client = new RestClient(_remoteAddress.Uri);
            var request = new RestRequest("/test");
            request.Timeout = (int)_webBinding.SendTimeout.TotalMilliseconds;
            var response = client.Execute<TestResponse>(request);
            return response.Data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longPollUrl"></param>
        /// <returns></returns>
        public TestResponse GetFriend(EndpointAddress longPollUrl)
        {
            InvokeWebserviceOperation<TestResponse> invokeGetFriend = new InvokeWebserviceOperation<TestResponse>(channel =>
            {
                return channel.GetFriend();
            });
            TestResponse response = null;
            doStuff(_longPollWebBinding, longPollUrl, invokeGetFriend, ref response);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TestResponse Test()
        {
            InvokeWebserviceOperation<TestResponse> invokeTest = new InvokeWebserviceOperation<TestResponse>(channel =>
            {
                    return channel.Test();
            });
            TestResponse response = null;
            doStuff(_webBinding, _remoteAddress, invokeTest, ref response);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="channel"></param>
        /// <returns></returns>
        private delegate T InvokeWebserviceOperation<T>(IFriendFindService channel);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="webHttpBinding"></param>
        /// <param name="endpointAddress"></param>
        /// <param name="work"></param>
        /// <param name="response"></param>
        private void doStuff<T>(WebHttpBinding webHttpBinding, EndpointAddress endpointAddress, InvokeWebserviceOperation<T> work, ref T response)
        {
            IChannelFactoryWrapper<IFriendFindService> factory = null;
            IFriendFindService channel = null;
            try
            {
                factory = _channelFactoryProvider.GetChannelFactory(webHttpBinding, endpointAddress);
                channel = factory.CreateChannel();
                response = work(channel);
            }
            catch
            {
                factory.Abort();
                throw;
            }
            finally
            {
                ((ICommunicationObject)channel).Close();
                factory.Close();
            }
        }

    }
}
