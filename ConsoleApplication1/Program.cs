﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace org.mardld.consoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            FriendFindServiceClient client = new FriendFindServiceClient("http://localhost:8080/friend-find-service/api/v1", 5, new RemoteChannelFactoryProvider<IFriendFindService>());
            TestResponse response = client.Test2();
            Console.WriteLine("Received " + response.Namekas);
            Console.ReadLine();

            try
            {
                TestResponse response2 = client.GetFriend(new System.ServiceModel.EndpointAddress("http://localhost:8080/friend-find-service/api/v1/friend?name=Albert"));
                Console.WriteLine("Received " + response2.Namekas);
            } finally
            {
                Console.ReadLine();
            }

        }
    }
}
