﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace org.mardld.consoleApp1
{
    public class RemoteChannelFactoryProvider<TChannel> : IChannelFactoryProvider<TChannel>
    {
        private IEndpointBehavior _endpointBehavior = new WebHttpBehavior();

        public IChannelFactoryWrapper<TChannel> GetChannelFactory(Binding binding, EndpointAddress remoteAddress)
        {
            ChannelFactory<TChannel> factory = new ChannelFactory<TChannel>(binding, remoteAddress);
            factory.Endpoint.Behaviors.Add(_endpointBehavior);
            return new ChannelFactoryWrapper<TChannel>(factory);
        }
    }
}
