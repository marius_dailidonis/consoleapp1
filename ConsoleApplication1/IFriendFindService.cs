﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace org.mardld.consoleApp1
{
    [ServiceContract]
    public interface IFriendFindService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/test", Method = "GET")]
        TestResponse Test();

        [OperationContract]
        [WebInvoke(UriTemplate = "", Method = "GET")]
        TestResponse GetFriend();
    }

    [DataContract]
    public class TestResponse
    {
        [DataMember(Name="name")]
        public string Namekas { get; set; }
    }
}
