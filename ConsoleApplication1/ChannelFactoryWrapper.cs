﻿using org.mardld.consoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace org.mardld.consoleApp1
{
    public class ChannelFactoryWrapper<TChannel> : IChannelFactoryWrapper<TChannel>
    {
        private readonly ChannelFactory<TChannel> _factory;

        public ChannelFactoryWrapper(ChannelFactory<TChannel> factory)
        {
            _factory = factory;
        }

        public void Abort()
        {
            _factory.Abort();
        }

        public void Close()
        {
            _factory.Close();
        }

        public TChannel CreateChannel()
        {
            return _factory.CreateChannel();
        }
    }
}
