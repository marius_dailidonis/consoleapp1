﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace org.mardld.consoleApp1
{
    [TestClass]
    public class FriendFindServiceClientTest
    {
        [TestMethod]
        public void TestTest()
        {
            // arrange
            string url = "http://test.com";
            int timeout = 10;

            TestResponse expected = MockRepository.GenerateMock<TestResponse>();

            MockedChannel channel = MockRepository.GenerateMock<MockedChannel>();
            channel.Expect(ch => ch.Test()).Return(expected);

            IChannelFactoryWrapper<IFriendFindService> factoryWrapper = MockRepository.GenerateMock<IChannelFactoryWrapper<IFriendFindService>>();
            factoryWrapper.Expect(w => w.CreateChannel()).Return(channel);

            IChannelFactoryProvider<IFriendFindService> channelFactoryProvider = MockRepository.GenerateMock<IChannelFactoryProvider<IFriendFindService>>();
            channelFactoryProvider.Expect(p => p.GetChannelFactory(Arg<Binding>.Is.NotNull, Arg<EndpointAddress>.Is.NotNull)).Return(factoryWrapper);

            FriendFindServiceClient client = new FriendFindServiceClient(url, timeout, channelFactoryProvider);


            // act
            TestResponse actual = client.Test();

            // assert
            Assert.AreEqual(expected, actual);
        }
    }

    public interface MockedChannel : IFriendFindService, ICommunicationObject
    {

    }
}
